﻿<?xml version="1.0" encoding="utf-8"?>
<Topic Id="ad9e62de-118e-43d6-86c1-43f86436b935" Status="New Topic" CreatedOn="2018-11-02T09:27:56.5186941Z" ModifiedOn="2020-07-29T16:34:47.9381041Z" PageTypeName="" AutoIndex="true" HelpContextIds="" Name="sparkline-types" BuildFlags="">
  <Title m="2018-11-02T09:29:33.6041624Z">Sparkline Types</Title>
  <ContentsTitle />
  <Notes></Notes>
  <TopicSections>
    <TopicSection Name="BodyText">
      <Content m="2020-07-29T16:34:47.9381041Z">&lt;P&gt;The Sparkline control supports three different sparkline types, namely Line, Column and Winloss, for visualizing data in different context. For example, Line charts are suitable to visualize continuous data, while Column sparklines are used in scenarios where data comparison is involved. Similarly, a Win-Loss sparkline is best used to visualize a true-false (that is, win-loss) scenario.&lt;/P&gt;
&lt;P&gt;The different sparkline types are explained in greater detail below:&lt;/P&gt;
&lt;TABLE&gt;
&lt;TBODY&gt;
&lt;TR&gt;
&lt;TH&gt;SparklineType&lt;/TH&gt;
&lt;TH&gt;Descriptions&lt;/TH&gt;&lt;/TR&gt;
&lt;TR&gt;
&lt;TD&gt;&lt;STRONG&gt;Line&lt;/STRONG&gt;&lt;BR&gt;&lt;IMG border=0 alt="Line type sparkline" src="images/Sparkline-ChartType-Line.png"&gt;&lt;/TD&gt;
&lt;TD&gt;A line sparkline consists of data points connected by line segments. It is best suited for visualizing continuous data and can be used to visualize sales figures, stock values or website traffic.&lt;BR&gt;By default, Sparkline renders as a line sparkline.&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;
&lt;TD&gt;&lt;STRONG&gt;Column&lt;BR&gt;&lt;IMG border=0 alt="column type sparkline" src="images/Sparkline-ChartType-Column.png"&gt;&lt;/STRONG&gt;&lt;/TD&gt;
&lt;TD&gt;Each data point in this type of sparkline is depicted as a vertical rectangle/column. In a column sparkline, positive data points are drawn in upward direction, while negative data points are drawn in downward direction. Column sparklines are used to facilitate comparison and are best suited for visualizing categorial data, for example, to visualize the revenues/profits earned from different departments in a store.&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;
&lt;TD&gt;&lt;STRONG&gt;WinLoss&lt;BR&gt;&lt;IMG border=0 alt="winloss sparkline" src="images/Sparkline-ChartType-WinLoss.png"&gt;&lt;/STRONG&gt;&lt;/TD&gt;
&lt;TD&gt;WinLoss sparkline displays data points through equal-sized columns drawn in upward and downward direction. The winloss sparkline is only concerned with whether a datapoint is positive or negative, it does not take the relative value of the data point into account. It is used to visualize a win/loss scenario. Columns drawn in upward direction indicate a win, while downward columns indicate a loss. For example, winloss sparkline can be used to track a sports season.&lt;/TD&gt;&lt;/TR&gt;&lt;/TBODY&gt;&lt;/TABLE&gt;
&lt;P&gt;The&amp;nbsp;$$C1Sparkline$$ class provides&amp;nbsp;$$SparklineType$$ property to set the sparkline type in designer or code. The SparklineType property accepts the following values from the SparklineType enumeration:&lt;/P&gt;
&lt;OL&gt;
&lt;LI&gt;&lt;STRONG&gt;Line&lt;/STRONG&gt; - Allows you to draw the line sparkline. 
&lt;LI&gt;&lt;STRONG&gt;Column&lt;/STRONG&gt; - Allows you to draw the column sparkline. 
&lt;LI&gt;&lt;STRONG&gt;WinLoss&lt;/STRONG&gt; - Allows you to draw the winloss sparkline. &lt;/LI&gt;&lt;/OL&gt;
&lt;P&gt;The code example below shows how you can set the SparklineType property to a specific sparkline type.&lt;/P&gt;&lt;innovasys:widget type="Example Code Tab Strip" layout="block"&gt;&lt;innovasys:widgetproperty layout="block" name="Content"&gt;&lt;innovasys:widget type="Colorized Example Code Section" layout="block"&gt;&lt;innovasys:widgetproperty layout="inline" name="LanguageName"&gt;VB&lt;/innovasys:widgetproperty&gt; &lt;innovasys:widgetproperty layout="block" name="Content"&gt;&lt;PRE&gt;&lt;CODE&gt;sparkline.SparklineType = SparklineType.Column&lt;/CODE&gt;
&lt;/PRE&gt;&lt;/innovasys:widgetproperty&gt;&lt;/innovasys:widget&gt;&lt;innovasys:widget type="Colorized Example Code Section" layout="block"&gt;&lt;innovasys:widgetproperty layout="inline" name="LanguageName"&gt;CS&lt;/innovasys:widgetproperty&gt; &lt;innovasys:widgetproperty layout="block" name="Content"&gt;&lt;PRE&gt;&lt;CODE&gt;sparkline.SparklineType = SparklineType.Column;&lt;/CODE&gt;
&lt;/PRE&gt;&lt;/innovasys:widgetproperty&gt;&lt;/innovasys:widget&gt;&lt;/innovasys:widgetproperty&gt;&lt;/innovasys:widget&gt;&lt;innovasys:widget type="Note Box" layout="block"&gt;&lt;innovasys:widgetproperty layout="block" name="Content"&gt;&lt;STRONG&gt;Note:&lt;/STRONG&gt; WinForms .NET 5 Edition has only runtime assemblies. Due to the new design-time model in VS2019 Preview, which is not complete yet from the Microsoft side, we do not supply any special design-time features as of yet. However, some of the controls might show up at design-time and allow editing few properties in the property grid.&lt;/innovasys:widgetproperty&gt; &lt;/innovasys:widget&gt;</Content>
    </TopicSection>
  </TopicSections>
  <TopicLinks />
  <TopicKeywords />
  <PropertyDefinitionValues>
    <PropertyDefinitionValue PropertyDefinitionId="b9775ea6-21f5-4f01-86f0-b95079af2d80">
      <PropertyValue m="2020-07-29T16:24:52.8979393Z">This topics lists the different types of sparkline.</PropertyValue>
    </PropertyDefinitionValue>
  </PropertyDefinitionValues>
  <ExcludedOtherFiles />
</Topic>