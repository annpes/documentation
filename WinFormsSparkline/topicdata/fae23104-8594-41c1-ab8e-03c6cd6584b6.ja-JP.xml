﻿<?xml version="1.0" encoding="utf-8"?>
<topic>
  <title m="2018-11-22T06:54:36.7223806Z">マーカー</title>
  <contentstitle m="2018-11-22T06:54:38.5629894Z">マーカー</contentstitle>
  <topicsection m="2019-08-12T07:04:03.9997242Z" name="BodyText">&lt;P&gt;Markers are symbols that are used to highlight or emphasize certain data points. With the help of markers, the sparkline becomes more readable and it becomes easier to distinguish between specific data points, such as high or low values.&lt;/P&gt;
&lt;P&gt;The Sparkline control supports markers only for the line sparkline type. In a line sparkline, markers are represented by the help of dot symbols which are shown on the data points. You can show all the data points in a line sparkline by setting the &lt;STRONG&gt;ShowMarkers&lt;/STRONG&gt; property provided by the &lt;STRONG&gt;C1Sparkline&lt;/STRONG&gt; class to &lt;STRONG&gt;true&lt;/STRONG&gt;. This highlights all the data points in brown color.&lt;/P&gt;
&lt;P&gt;The following example code shows how you can set the ShowMarkers property.&lt;/P&gt;
&lt;DIV class=notranslate&gt;&lt;innovasys:widget type="Colorized Example Code" layout="block"&gt;&lt;innovasys:widgetproperty layout="inline" name="Title"&gt;C#&lt;/innovasys:widgetproperty&gt; &lt;innovasys:widgetproperty layout="inline" name="LanguageName"&gt;CS&lt;/innovasys:widgetproperty&gt; &lt;innovasys:widgetproperty layout="block" name="Content"&gt;&lt;PRE&gt;&lt;CODE&gt;//Highlights all the data points
sparkline.ShowMarkers = true;&lt;/CODE&gt;
&lt;/PRE&gt;&lt;/innovasys:widgetproperty&gt;&lt;/innovasys:widget&gt;&lt;/DIV&gt;
&lt;P&gt;The &lt;STRONG&gt;ShowMarkers&lt;/STRONG&gt; property highlights all the data points in Sparkline. However, some users might want to highlight some specific values to suit their business needs. For this, the Sparkline control allows users to apply markers on specific data points as well. The following properties can be set in code for the same.&lt;/P&gt;
&lt;TABLE&gt;
&lt;TBODY&gt;
&lt;TR&gt;
&lt;TD width=92&gt;&lt;STRONG&gt;Property&lt;/STRONG&gt;&lt;/TD&gt;
&lt;TD&gt;&lt;STRONG&gt;Description&lt;/STRONG&gt;&lt;/TD&gt;
&lt;TD&gt;&lt;STRONG&gt;Output&lt;/STRONG&gt;&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;
&lt;TD width=92&gt;&lt;A href="C1.Win.Sparkline.4.5.2~C1.Win.Sparkline.C1Sparkline~ShowFirst.html"&gt;ShowFirst&lt;/A&gt;&lt;/TD&gt;
&lt;TD&gt;You can set this property to true in code to highlight the first data point in the output as shown in the image alongside. By default, the marker highlights the first data point in maroon color.&lt;/TD&gt;
&lt;TD&gt;&lt;IMG border=0 alt="Sparkline showing first data point" src="images/Sparkline-FirstMarker.png"&gt;&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;
&lt;TD width=92&gt;&lt;A href="C1.Win.Sparkline.4.5.2~C1.Win.Sparkline.C1Sparkline~ShowLast.html"&gt;ShowLast&lt;/A&gt;&lt;/TD&gt;
&lt;TD&gt;You can set this property to true in code to highlight the last data point in the output as shown in the image alongside. By default, the marker highlights the last data point in green color.&lt;/TD&gt;
&lt;TD&gt;&lt;IMG border=0 alt="Sparkline showing last data point" src="images/Sparkline-LastMarker.png"&gt;&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;
&lt;TD width=92&gt;&lt;A href="C1.Win.Sparkline.4.5.2~C1.Win.Sparkline.C1Sparkline~ShowHigh.html"&gt;ShowHigh&lt;/A&gt;&lt;/TD&gt;
&lt;TD&gt;You can set this property to true in code to highlight the highest value in the output as shown in the image alongside. By default, the marker highlights the highest data point in red color.&lt;/TD&gt;
&lt;TD&gt;&lt;IMG border=0 alt="Sparkline showing highest data point" src="images/Sparkline-HighMarker.png"&gt;&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;
&lt;TD width=92&gt;&lt;A href="C1.Win.Sparkline.4.5.2~C1.Win.Sparkline.C1Sparkline~ShowLow.html"&gt;ShowLow&lt;/A&gt;&lt;/TD&gt;
&lt;TD&gt;You can set this property to true in code to highlight the lowest value in the output as shown in the image alongside. By default, the marker highlights the lowest data point in blue color.&lt;/TD&gt;
&lt;TD&gt;&lt;IMG border=0 alt="Sparkline showing lowest data point" src="images/Sparkline-LowMarker.png"&gt;&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;
&lt;TD width=92&gt;&lt;A href="C1.Win.Sparkline.4.5.2~C1.Win.Sparkline.C1Sparkline~ShowNegative.html"&gt;ShowNegative&lt;/A&gt;&lt;/TD&gt;
&lt;TD&gt;You can set this property to true in code to highlight all the negative data points in the output as shown in the image alongside. By default, the marker highlights the negative data point(s) in red color.&lt;/TD&gt;
&lt;TD&gt;&lt;IMG border=0 alt="" src="images/Sparkline-NegativeMarker.png"&gt;&lt;/TD&gt;&lt;/TR&gt;&lt;/TBODY&gt;&lt;/TABLE&gt;
&lt;H3&gt;Customizing the color of Data Points&lt;/H3&gt;
&lt;P&gt;As mentioned above, various data points are rendered in a default color. However, Sparkline allows you to modify this default behavior and highlight data points in a color of your choice, making Sparkline more customizable for users from appearance perspective.&lt;/P&gt;
&lt;P&gt;The following image shows how a Sparkline control appears after setting the MarkersColor property to Red.&lt;/P&gt;
&lt;P align=left&gt;&lt;IMG border=0 alt="" src="images/Sparkline-MarkerColor.png"&gt;&lt;/P&gt;
&lt;P&gt;The following code example shows how to customize the markers in the Sparkline control.&lt;/P&gt;
&lt;DIV class=notranslate&gt;&lt;innovasys:widget type="Colorized Example Code" layout="block"&gt;&lt;innovasys:widgetproperty layout="inline" name="Title"&gt;C#&lt;/innovasys:widgetproperty&gt; &lt;innovasys:widgetproperty layout="inline" name="LanguageName"&gt;CS&lt;/innovasys:widgetproperty&gt; &lt;innovasys:widgetproperty layout="block" name="Content"&gt;&lt;PRE&gt;&lt;CODE&gt;//Setting Marker Color
sparkline.MarkersColor = Colors.Red;&lt;/CODE&gt;
&lt;/PRE&gt;&lt;/innovasys:widgetproperty&gt;&lt;/innovasys:widget&gt;&lt;/DIV&gt;
&lt;P&gt;Following is the list of properties that can be used to set the colors for highlighting specific data points.&lt;/P&gt;
&lt;TABLE&gt;
&lt;TBODY&gt;
&lt;TR&gt;
&lt;TD width=224&gt;&lt;STRONG&gt;Property&lt;/STRONG&gt;&lt;/TD&gt;
&lt;TD&gt;&lt;STRONG&gt;Description&lt;/STRONG&gt;&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;
&lt;TD width=224&gt;&lt;A href="C1.Win.Sparkline.4.5.2~C1.Win.Sparkline.SparklineTheme~FirstMarkerColor.html"&gt;FirstMarkerColor&lt;/A&gt;&lt;/TD&gt;
&lt;TD&gt;This property can be set to specify the marker color for the first data point in Sparkline.&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;
&lt;TD width=224&gt;&lt;A href="C1.Win.Sparkline.4.5.2~C1.Win.Sparkline.SparklineTheme~LastMarkerColor.html"&gt;LastMarkerColor&lt;/A&gt;&lt;/TD&gt;
&lt;TD&gt;This property can be set to specify the marker color for the last data point in Sparkline.&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;
&lt;TD width=224&gt;&lt;A href="C1.Win.Sparkline.4.5.2~C1.Win.Sparkline.SparklineTheme~HighMarkerColor.html"&gt;HighMarkerColor&lt;/A&gt;&lt;/TD&gt;
&lt;TD&gt;This property can be set to specify the marker color for the highest data point in Sparkline.&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;
&lt;TD width=224&gt;&lt;A href="C1.Win.Sparkline.4.5.2~C1.Win.Sparkline.SparklineTheme~LowMarkerColor.html"&gt;LowMarkerColor&lt;/A&gt;&lt;/TD&gt;
&lt;TD&gt;This property can be set to specify the marker color for the lowest data point in Sparkline.&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;
&lt;TD width=224&gt;&lt;A href="C1.Win.Sparkline.4.5.2~C1.Win.Sparkline.SparklineTheme~NegativeColor.html"&gt;NegativeColor&lt;/A&gt;&lt;/TD&gt;
&lt;TD&gt;This property can be set to specify the marker color for negative data points in Sparkline.&lt;/TD&gt;&lt;/TR&gt;&lt;/TBODY&gt;&lt;/TABLE&gt;</topicsection>
</topic>